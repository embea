require File.dirname(__FILE__) + '/../spec_helper'

describe UsersController do
  fixtures :users, :roles, :roles_users

  it "should show users index" do
    users(:admin).log_in
    get :index
    response.should render_template(:index)
  end

  it "should deny anonymous users" do
  end
end

require File.dirname(__FILE__) + '/../spec_helper'

describe SessionsController do
  fixtures :users, :roles, :roles_users

  it "should allow logging in" do
    post :create, :user => { :login => "admin", :password => "admin" }
    response.should be_redirect
    response.should redirect_to(home_url)
    assigns(:current_user).should == User.find_by_id(1)
  end

  it "should not log in with invalid pass" do
    post :create, :user => { :login => "admin", :password => "foobar" }
    response.should be_redirect
    response.should redirect_to(home_url)
    assigns(:current_user).should == nil
  end

  it "should allow logging out" do
    users(:admin).log_in
    post :destroy
    response.should be_redirect
    response.should redirect_to(home_url)
    assigns(:current_user).should == nil
  end
end

# This file is copied to ~/spec when you run 'ruby script/generate rspec'
# from the project root directory.
ENV["RAILS_ENV"] ||= "test"
require File.expand_path(File.dirname(__FILE__) + "/../config/environment")
require 'spec/rails'

Spec::Runner.configure do |config|
  config.use_transactional_fixtures = true
  config.use_instantiated_fixtures  = false
  config.fixture_path = RAILS_ROOT + '/spec/fixtures'
  config.before(:each, :behaviour_type => :controller) do
    raise_controller_errors

    req = lambda { request }

    User.send :define_method, :log_in do
      req.call.session[:user_id] = self.id
    end
  end
end

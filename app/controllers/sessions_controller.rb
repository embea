class SessionsController < ApplicationController
  def create
    user = User.login(params[:user][:login], params[:user][:password])

    unless user
      flash[:error] = "Invalid login and/or password, please try again"
      redirect_to home_url and return
    end

    session[:user_id] = user.id
    @current_user = user
    flash[:notice] = "Successfuly logged in"
    redirect_to home_url
  end

  def destroy
    session[:user_id] = nil
    redirect_to home_url
  end
end

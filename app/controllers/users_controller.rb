class UsersController < ApplicationController
  before_filter :get_user, :require_user, :only_for_admins

  def index
    @users = User.find(:all, :order => "id ASC")
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end
end

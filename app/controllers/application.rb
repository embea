# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  session :session_key => '_embea_session_id'

  layout "embea"

  alias :missing_method :method_missing

  def method_missing(method_sym)
    looking_for = method_sym.to_s[/^embea_find_(.*)$/, 1]
    return model_find(looking_for) if looking_for
    return missing_method(method_sym)
  end

  protected

  def embea_model_find(model)
    return missing if !params[:id]
    klass = Kernel.const_get(model.capitalize)
    obj = klass.find_by_id(params[id])
    instance_variable_set("@#{model}", obj)
    return missing if !obj
  end

  def missing(message = "404 Not Found")
    render :text => message, :status => 404
  end

  def forbidden(message = "403 Forbidden")
    render :text => message, :status => 403
  end

  def get_user
    @current_user = User.find_by_id(session[:user_id])
  end

  def require_user
    unless @current_user
      redirect_to :controller => 'forums', :action => 'index' and return
    end
  end

  def only_for_admins
    return forbidden unless @current_user.is?(:admin)
  end
end

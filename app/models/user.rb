require 'digest/sha1'

class User < ActiveRecord::Base
  attr_accessor :password
  has_and_belongs_to_many :roles, :uniq => true

  def self.login(login, password)
    User.find_by_login_and_password_hash(login, User.crypt(password))
  end

  def before_save
    self.password_hash = User.crypt(self.password) if self.password
    self.password = ""
  end

  def self.crypt(password)
    Digest::SHA1.hexdigest(password)
  end

  def is?(role_sym)
    self.roles.include?(Role[role_sym])
  end
end

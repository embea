class AddDefaultUser < ActiveRecord::Migration
  def self.up
    Role.reset_column_information

    admin = User.new
    admin.login = "admin"
    admin.email = "admin@administration.com"
    admin.password_hash = "d033e22ae348aeb5660fc2140aec35850c4da997" # Default pass is "admin"
    admin.roles << Role[:admin]
    admin.save!
  end

  def self.down
    User.find_by_email("admin@administration.com").destroy!
  end
end

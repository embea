class AddBasicRoles < ActiveRecord::Migration
  def self.up
    Role.enumeration_model_updates_permitted = true

    admin = Role.new
    admin.name = "admin"
    admin.description = "Administration"
    admin.save!

    moderator = Role.new
    moderator.name = "moderator"
    moderator.description = "Moderator"
    moderator.save!
  end

  def self.down
    Role.find_by_name("admin").destroy
    Role.find_by_name("moderator").destroy
  end
end
